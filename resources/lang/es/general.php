<?php

return [
    'ok'   => 'ok.',
    'justnow'   => 'Justo ahora',
    'aminute' => "Hace un minuto",
    'timeago' => "Hace",
    'timeminutes' => "minutos",
    'ahour' => "Hace una hora",
    'timehours' => "horas",
    'yesterday' => "Ayer",
    'timedays' => "días",
    'week' => "Hace una semana",
    'timeweek' => "semanas",
    'month' => "Hace un mes",
    'timemonth' => "meses",
    'year' => "Hace un año",
    'timeyear' => "años",


];
