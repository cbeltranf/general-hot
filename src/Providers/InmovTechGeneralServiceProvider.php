<?php

namespace Inmovsoftware\GeneralApi\Providers;

use Illuminate\Support\ServiceProvider;
use Inmovsoftware\GeneralApi\Http\Resources\V1\GlobalCollection;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Inmovsoftware\GeneralApi\Models\V1\Logs;
use DB;
use Log;
use Carbon\Carbon;

class InmovTechGeneralServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');

        $this->publishes([
            __DIR__.'/../../resources/lang' => resource_path('/lang'),
                ], 'generalLangs');

        Artisan::call('vendor:publish' , [
                      '--tag' => 'generalLangs',
                    '--force' => true,
        ]);

        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'general');

        $this->app['events']->listen('artisan.start', function(){
            DB::enableQueryLog();
        });



    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $env_update = $this->changeEnv([
            'MAIL_DRIVER'=>'smtp',
            'MAIL_HOST'=>'smtp.sendgrid.net',
            'MAIL_PORT'=>'587',
            'MAIL_USERNAME'=>'devinmov',
            'MAIL_PASSWORD'=>'22222',
            'MAIL_API'=>'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',
            'MAIL_ENCRYPTION'=>'tls',
            'MAIL_FROM_NAME'=>'"My_Digital_Card"',
            'MAIL_FROM_ADDRESS'=>'"noreply@mydigitalcard.us"',
            'TWILIO_AUTH_TOKEN'   => '6929e00896ac5f699a85ac88f2687810',
            'TWILIO_ACCOUNT_SID'   => 'ACdc935164b11de2860ce4f34b0f77b8b3',
            'TWILIO_NUMBER'       => '+14697784007']);




        $this->loadMiddleware();
        $this->app->make('Inmovsoftware\GeneralApi\Models\V1\Logs');
        $this->app->make('Inmovsoftware\GeneralApi\Models\V1\Appinfo');
       // $this->app->make('Inmovsoftware\GeneralApi\Traits\V1\helper');
        $this->app->make('Inmovsoftware\GeneralApi\Http\Controllers\V1\GeneralController');
        $this->registerHandler();

    }


    protected function registerHandler()
    {
        \App::singleton(
            Illuminate\Http\Resources\Json\ResourceCollection::class,
            GlobalCollection::class
        );

        $this->app->register('Intervention\Image\ImageServiceProvider');
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('Image', 'Intervention\Image\Facades\Image');

    }

    protected function loadMiddleware()
    {

        $kernel = $this->app->make('Illuminate\Contracts\Http\Kernel');
        $kernel->pushMiddleware('Inmovsoftware\GeneralApi\Http\Middleware\SaveLogInmov');

    }

    protected function changeEnv($data = array()){
        if(count($data) > 0){

            // Read .env-file
            $env = file_get_contents(base_path() . '/.env');

            // Split string on every " " and write into array
            $env = preg_split('/\s+/', $env);
/*
            Log::error(
                'ERR data ' .print_r($data, true)
            );
            Log::error(
                'ERR env ' .print_r($env, true )
            );
*/
            $temp ='';
            foreach((array)$data as $key => $value){
                foreach($env as $env_key => $env_value){
                    if(!empty(trim($env_value))){
                    $temp = explode("=", $env_value, 2);
                        $to_analyze[$temp[0]] = $temp[1];
                    }

                }
            }

/*            Log::error(
                'To analyze  ' .print_r($to_analyze, true )
            );

*/
            // Loop through given data
            foreach((array)$data as $key => $value){

                if (! array_key_exists($key, $to_analyze)) {
                    $env[$key] = $key . "=" . $value;
                                    }

                // Loop through .env-data
                foreach($env as $env_key => $env_value){


                    // Turn the value into an array and stop after the first split
                    // So it's not possible to split e.g. the App-Key by accident
                    $entry = explode("=", $env_value, 2);

                    // Check, if new key fits the actual .env-key
                    if($entry[0] == $key){
                        // If yes, overwrite it with the new one
                        $env[$env_key] = $key . "=" . $value;
                    } else {
                        // If not, keep the old one
                        $env[$env_key] = $env_value;
                    }
                }
            }

            // Turn the array back to an String
            $env = implode("\n", $env);

            // And overwrite the .env with the new data
            file_put_contents(base_path() . '/.env', $env);
            return true;
        } else {
            return false;
        }
    }


    public function commented_method(){

        /*
        DB::listen(function($query) {

            $Auth_user = auth('api')->user();
            if(isset($Auth_user->id) && !empty($Auth_user->id)){

            Log::error(
                'In  y usr'
            );
dd("here". $query->sql);
                if( strpos(strtolower($query->sql), "it_logs") === false && strpos(strtolower($query->sql), "select")  === false  && strpos(strtolower($query->sql), " - Params: ")  === false ){

                    Log::error(
                        'In cond '
                    );

                    $log = new Logs;
                    $log->it_business_id = $Auth_user->it_business_id;
                    $log->it_users_id = $Auth_user->id;
                    $log->date = Carbon::now()->format('Y-m-d H:i:s');
                    $log->descriptions = 'Query: '.$query->sql. ' - Params: '. json_encode($query->bindings). ' Time: '. $query->time;
                    $log->save();

                }else{
                    Log::error(
                        ' No entrance '
                    );
                }

            }


        });
*/

    }



}
