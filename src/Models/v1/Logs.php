<?php

namespace Inmovsoftware\GeneralApi\Models\V1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Logs extends Model
{
    use SoftDeletes;
    protected $table = "it_logs";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
    protected $fillable = ['it_business_id', 'it_users_id', 'date','process','descriptions'];


}
