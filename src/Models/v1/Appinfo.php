<?php

namespace Inmovsoftware\GeneralApi\Models\V1;

use Illuminate\Database\Eloquent\Model;

class Appinfo extends Model
{
    protected $table = "it_appinfo";
    protected $primaryKey = 'type';
    protected $guarded = ['type'];
    protected $fillable = ['type', 'extra', 'link_update'];

}
