<?php
namespace Inmovsoftware\GeneralApi\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Inmovsoftware\GeneralApi\Models\V1\Logs;
use Inmovsoftware\GeneralApi\Models\V1\Appinfo;
use Inmovsoftware\GeneralApi\Http\Resources\V1\GlobalCollection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Carbon\Carbon;
use SendGrid\Mail\Mail as Mail;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Image;


use SendGrid as SendGrid;

class GeneralController extends Controller
{
    public function index(Request $request)
    {

    }


    public function store(Request $request)
    {

    }

    public function app_store(Request $request)
    {

    }


    public function show($id)
    {

    }

    public function update(Request $request, $id)
    {

    }


    public function destroy($id)
    {

    }

    public function app_version(Request $request)
    {
        $data = $request->validate([
            "platform" => "required|in:android,ios"
        ]);

        $app = Appinfo::find($data["platform"]);
        return response()->json($app);

    }


    public function interlacing_image($folder, $img)
    {
    }

    public function save_interlacing($folder, $img)
    {
        $real_image_path = $folder.'/'.$img;
        $image_path = public_path($real_image_path);

        $x = explode(".", $image_path);
        $i = count($x);
        $indexName = $i-2;
        $x[$indexName] =  $x[$indexName].'-_-_-_-_-_-_-_-_-_-_old-_-_-_-_-_-_-_-_-_-_';
        $old_img = implode(".", $x);

        rename($image_path, $old_img);



      Image::make($old_img)->interlace()->save($image_path, 100);
      File::delete($old_img);

    }


}
