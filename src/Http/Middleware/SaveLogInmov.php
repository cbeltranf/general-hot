<?php

namespace Inmovsoftware\GeneralApi\Http\Middleware;

use Closure;
use Inmovsoftware\GeneralApi\Models\V1\Logs;
use DB;
use Log;
use Carbon\Carbon;

class SaveLogInmov
{

    public function handle($request, Closure $next)
    {
        DB::enableQueryLog();
        return $next($request);
    }

    public function terminate($request, $response)
    {
        $Auth_user = auth('api')->user();
        $to_save = '';

        if(isset($Auth_user->id) && !empty($Auth_user->id))
        {
            $items = DB::getQueryLog();
            $log = json_encode($items);


              foreach($items as $item){
                if( strpos(strtolower($item["query"]), "it_logs") === false   && strpos(strtolower(json_encode($item["query"])), "select")  === false ){
                        $to_save.="{Query: ".$item["query"].", Data: ".json_encode($item["bindings"])."},  ";
                    }
              }

              if( $to_save != '' ){
                    $log = new Logs;
                    $log->it_business_id = $Auth_user->it_business_id;
                    $log->it_users_id = $Auth_user->id;
                    $log->date = Carbon::now()->format('Y-m-d H:i:s');
                    $log->descriptions = $to_save;
                    $log->save();
              }
            }

    }
}
