<?php
use Illuminate\Http\Request;

//v1
Route::middleware(['api'])->group(function () {

Route::get('image/{folder}/{img}', 'Inmovsoftware\GeneralApi\Http\Controllers\V1\GeneralController@interlacing_image');

Route::get('image/test_interlace/{folder}/{img}', 'Inmovsoftware\GeneralApi\Http\Controllers\V1\GeneralController@save_interlacing');

Route::group([
    'prefix' => 'api/v1'
], function () {

    Route::post('app/version/info', 'Inmovsoftware\GeneralApi\Http\Controllers\V1\GeneralController@app_version');
   // Route::apiResource('company', 'Inmovsoftware\CompanyApi\Http\Controllers\V1\CompanyController');
          });
});

Route::middleware(['api', 'jwt'])->group(function () {
    Route::group([
        'prefix' => 'api/v1'
    ], function () {

       // Route::apiResource('company', 'Inmovsoftware\CompanyApi\Http\Controllers\V1\CompanyController');
              });
    });


