<?php
namespace Inmovsoftware\GeneralApi\Traits\V1;

use Inmovsoftware\GeneralApi\Http\Resources\V1\GlobalCollection;
use Berkayk\OneSignal\OneSignalClient as One;
use Twilio\Exceptions\TwilioException;
use SendGrid\Mail\Mail as Mail;
use Illuminate\Http\Request;
use SendGrid as SendGrid;
use Twilio\Rest\Client;
use Carbon\Carbon;
use Exception;
use Log;
use DB;

trait helper {

    public function implode_collection($collection, $field){
        $respose = '';
        foreach($collection as $item){
            $respose.= trim($item->{$field}).',';
        }
        return rtrim($respose,',');
    }

    public function get_user_language(Request $request){

        $local =  ($request->hasHeader('X-Language')) ? $request->header('X-Language') : 'en';
        return strtolower($local);
    }

    public function check_free_email($email){
        $email = strtolower($email);
        $free_mails = array("gmail", "hotmail", "gmx", "aol", "outlook", "protonmail", "yahoo", "mail", "inbox", "zoho", "shortmail", "hushmail", "ingenieros");

        $alias = explode("@", $email);
        $alias = explode(".", $alias[1]);
        $alias = $alias[0];

        if(in_array($alias, $free_mails)){
          return true;
        }else{
            return false;
        }
    }

    public function time_ago($timestamp)  {
        $time_ago = strtotime($timestamp);
        $current_time = time();
        $time_difference = $current_time - $time_ago;
        $seconds = $time_difference;
        $minutes      = round($seconds / 60 );           // value 60 is seconds
        $hours           = round($seconds / 3600);           //value 3600 is 60 minutes * 60 sec
        $days          = round($seconds / 86400);          //86400 = 24 * 60 * 60;
        $weeks          = round($seconds / 604800);          // 7*24*60*60;
        $months          = round($seconds / 2629440);     //((365+365+365+365+366)/5/12)*24*60*60
        $years          = round($seconds / 31553280);     //(365+365+365+365+366)/5 * 24 * 60 * 60
        if($seconds <= 60)
        {
    return  trans('general.justnow');
    }
        else if($minutes <=60)
        {
    if($minutes==1)
            {
        return trans('general.aminute');
    }
    else
            {
        return    trans('general.timeago')." $minutes ".trans('general.timeminutes');
    }
    }
        else if($hours <=24)
        {
    if($hours==1)
            {
        return trans('general.ahour');
    }
            else
            {
        return trans('general.timeago')." $hours ".trans('general.timehours');
    }
    }
        else if($days <= 7)
        {
    if($days==1)
            {
        return trans('general.yesterday');
    }
            else
            {
        return trans('general.timeago')." $days ".trans('general.timedays');
    }
    }
        else if($weeks <= 4.3) //4.3 == 52/12
        {
    if($weeks==1)
            {
        return trans('general.week');
    }
            else
            {
        return trans('general.timeago')." $weeks ".trans('general.timeweek');
    }
    }
        else if($months <=12)
        {
    if($months==1)
            {
        return trans('general.month');
    }
            else
            {
        return  trans('general.timeago')." $months ".trans('general.timemonth');
    }
    }
        else
        {
    if($years==1)
            {
        return trans('general.year');
    }
            else
            {
        return trans('general.timeago')." $years ".trans('general.timeyear');
    }
    }
    }


    public function get_country_from_iso($iso){
       $query =  DB::table("it_countries")
                    ->select("id")
                    ->where("iso", '=', $iso)->get();

        return $query[0]->id;

    }

    public function users_to_notify($groups = array(), $branchs = array(), $positions = array(), $users = array(), $platforms = array(), $type = 'email'){//uuid
        //$platform Android iOS
        $group ="'NULL'";
        $branch ="'NULL'";
        $user ="'NULL'";
        $position ="'NULL'";
        $platform ="'NULL'";

        foreach($groups as $k => $v){
            $group .= "'$v',";
        }
        foreach($branchs as $k => $v){
            $branch .="'$v',";
        }
        foreach($users as $k => $v){
            $user .="'$v',";
        }
        foreach($positions as $k => $v){
            $position .="'$v',";
        }
        foreach($platforms as $k => $v){
            $platform .="'$v',";
        }
        $query =  DB::select("
        SELECT DISTINCT($type) as item FROM `it_users` AS us
        INNER JOIN `it_user_group` AS ug ON ug.`it_users_id` = us.`id`
        INNER JOIN `it_groups_users` AS gr ON gr.`id` = ug.`it_groups_users_id`
        WHERE gr.id in(".rtrim($group,",").") AND us.`status` = 'A' AND us.`uuid` IS NOT NULL
        UNION DISTINCT
        SELECT DISTINCT($type) as item FROM `it_users` AS us
        INNER JOIN `it_branches` AS br ON br.`id` = us.`it_branches_id`
        WHERE br.id  in(".rtrim($branch,",").") AND us.`status` = 'A' AND us.`uuid` IS NOT NULL
        UNION DISTINCT
        SELECT DISTINCT($type) as item FROM `it_users` AS us
        WHERE us.`id` in(".rtrim($user,",").")
        UNION DISTINCT
        SELECT DISTINCT($type) as item FROM `it_users` AS us
        INNER JOIN `it_positions` AS po ON po.`id` = us.`it_positions_id`
        WHERE po.id  in(".rtrim($position,",").") AND us.`status` = 'A' AND us.`uuid` IS NOT NULL
        UNION DISTINCT
        SELECT DISTINCT(email) FROM `it_users` AS us
        WHERE platform in(".rtrim($platform,",").") AND us.`status` = 'A'

        ");

        $return = array();

        foreach($query as $k => $v){
            $return[]=$v->item;
        }

        return $return;
 }

     public function send_push_notification($uuids, $section, $item_id, $title, $content){

            try{
        $parameters = array(
            "include_player_ids" => $uuids,
            "app_id" => "b5baa53e-d6a0-4752-be32-b36eede1b087",
            "contents" => array("en"=> $content, "es"=> $content),
            "headings" => array("en" => $title, "es"=> $title),
            "subtitle" => array("en" => "", "es"=> ""),
            "android_led_color" => "FF9900FF",
            "test_type" => "1",
            "data" => array("section"=>$section, "item_id"=> $item_id)
            );

            $message =  new One(env("OS_APP_ID"), env("OS_REST_API"), env("OS_AUTH"));
            $message->sendNotificationCustom($parameters);
            return true;
            }catch(Exception $ex){
                return response()->json($ex);

                         }
     }


     public function my_news(){

            $Auth_user = auth('api')->user();

            $allowedNews = array();
            $groupsUser = array();

            $news_all  = DB::table("it_news")->whereNull('deleted_at')
            ->where("status", "=", "A")
            ->where('it_news.id_business_id', '=', $Auth_user->it_business_id)
            ->whereRaw('(((publication_date IS NOT NULL AND NOW() >= publication_date ) OR publication_date IS NULL )
            AND (
                  (close_date IS NOT NULL AND NOW() < DATE(close_date)  )
                OR close_date IS NULL )) ')
            ->select("id", "post_to")->get();

            $my_groups = DB::table("it_user_group")->whereNull('deleted_at')
            ->where('it_users_id', '=', $Auth_user->id)
            ->select("it_groups_users_id")->get();

            foreach($my_groups as $gr){
                array_push($groupsUser, $gr->it_groups_users_id);
            }



            foreach($news_all as $item){
               $pass = true;
               //echo  $item->id;
               $filtros = json_decode($item->post_to);

              $branch = $filtros->branches_to_publish;
              $groups = $filtros->groups_to_publish;
              $position = $filtros->position_to_publish;


               if(!empty($groups) && !array_intersect($groupsUser, $groups)){
                $pass = false;
               }

               if(!empty($branch) && !in_array($Auth_user->it_branches_id, $branch)){
                $pass = false;
               }

               if(!empty($position) && !in_array($Auth_user->it_positions_id, $position)){
                $pass = false;
               }

               if(empty($groups) && empty($branch) && empty($position)){
                $pass = true;
               }

               if($pass == true){
                array_push($allowedNews, $item->id);
               }

            }

            return $allowedNews;

     }

}
